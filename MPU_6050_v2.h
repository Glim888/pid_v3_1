#ifndef __MPU6050_V2__
#define __MPU6050_V2__

#include <Arduino.h>
#include <Wire.h>
#include "PID_v3.h"
#include <EEPROM.h>

#define ADDRESS 0x68          // MPU 6050 I2C Adresse
#define OFFSET_STEPS 3000     // Kalibrier Stepps 
#define G_IN_VALUE 16384.     // 16384 bedeutet, das 1 G wirkt (bei +-2g)
#define DEG_IN_VALUE 131.     // 131 bedeutet, das um 1° gedreht wurde (bei +-250°)
#define FREQUENCY 500         // Timerinterrupt Frequenz
#define FILTER_ACC 0.01       // complementary  filter
#define FILTER_GYRO 0.99      // complementary  filter
#define TEST_FREQUENZY 1000   // Zeit zwischen Testausgaben (in Milli)

class MPU_6050_v2 {

  private:

    float ac_xOffset, ac_yOffset, ac_zOffset, gy_xOffset, gy_yOffset, gy_zOffset;
    float ac_x, ac_y, ac_z, gy_x, gy_y, gy_z;
    float angle = 0;
    float lastTime = 0;
    long ac_xSum = 0, ac_ySum = 0, ac_zSum = 0, gy_xSum = 0, gy_ySum = 0, gy_zSum = 0;
    int offset_counter = 0;

    void get_info();

  public:

    MPU_6050_v2 ();

    // MPU initialisieren
    void init();

    // Register auslesen
    byte get_register(byte _register);

    // Register setzen
    void set_register(byte _register, byte _data);

    // Ausgabe der Sensorwerte
    void test();

    // Rohdaten auslesen
    void get_rawData();

    // Werte auslesen im Maßeinheitenformat
    void get_data();

    // MPU kalibrieren
    void calibrate(int setAccX, int setAccY, int setAccZ);

    // Offsets aus EEPROM laden
    void loadOffsetsFromEEPROM();

    // Winkel auslesen
    float get_angle();

    // Beschleunigungswinkel auslesen
    float get_acc_angle();

    // ändern des Gyro Winkels
    void set_gyro_angle(float _angle);
};

#endif
