#include "PID.h"

pid::pid (float _kp, float _ki, float _kd, float *_setpoint, float _limit) {
  kp = _kp;
  ki = _ki;
  kd = _kd;
  setpoint = _setpoint;
  set_limit(_limit);
}


float pid::calc(float _angle) {

  float _diff_time = (millis() - lastTime) * 0.001;
  float _diff_p = _angle - *setpoint;
  float _diff_d = _angle - last_d;
  float _return = 0;

  sum_i += ki * _diff_p * _diff_time;

  sum_i = max (sum_i, -5 * limit_minMax);
  sum_i = min (sum_i, 5 * limit_minMax);

  _return += kp * _diff_p;
  _return += sum_i;
  if (_diff_time != 0.0) _return -= (kd * _diff_d) / _diff_time;

  _return = max (_return, -limit_minMax);
  _return = min (_return, limit_minMax);

  last_d = _angle;
  lastTime = millis();

  return _return;
}


float pid::get_P() {
  return kp;
}
float pid::get_I() {
  return ki;
}
float pid::get_D() {
  return kd;
}

float pid::get_limit() {
  return limit_minMax;
}

void pid::set_pid(float _p, float _i, float _d) {
  set_P(_p);
  set_I(_i);
  set_D(_d);
}

void pid::set_P(float _p) {
  kp = _p;
}
void pid::set_I(float _i) {
  ki = _i;
}
void pid::set_D(float _d) {
  kd = _d;
}
void pid::set_limit(float _minMax) {
  limit_minMax = _minMax;
}



