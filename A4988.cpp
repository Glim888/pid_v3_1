#include "A4988.h"

volatile unsigned int ISR_i = 0;
volatile unsigned int a4988_waitTime = 0;
volatile unsigned long deltaStepps = 0;

A4988::A4988(int pinDir, int pinStepps) {

  this->pinDir = pinDir;
  this->pinStepps = pinStepps;

  pinMode(pinDir, OUTPUT);
  pinMode(pinStepps, OUTPUT);

  a4988_dir = STOP;
  deltaStepps = 0;
  set_sumStepps(0);
}


void A4988::init() {
  // Timer
  noInterrupts();
  TCCR4A = 0;
  TCCR4B = 0;
  TCNT4 = 0;

  OCR4A = 5;               //  25 ->0.1 Milli
  TCCR4B |= (1 << WGM12);   // CTC Mode -> Reset der TimerVal zu 0, wenn TIMER4_COMPA_vect erfüllt ist
  TCCR4B |= (1 << CS10);    // 64 Prescaler
  TCCR4B |= (1 << CS11);    // 64 Prescaler
  TIMSK4 |= (1 << OCIE4A);
  interrupts();

  set_sumStepps(0);
}



void A4988::setMove(int spd, int dir) {

  // a4988_waitTime = 1000000 / spd / 800 - 1; ALT

  // ermitteln der zu wartenden ISR Zyklen, bevor der Output Status geändert wird
  a4988_waitTime = 50000 / spd; // SPD entspricht Stepps pro Sekunde (default: 10000)

  //Serial.println(a4988_waitTime);

  update_sumStepps();

  a4988_dir = dir;

  // Takt deaktivieren
  if ((dir == STOP) || (spd == 0)) {
    a4988_waitTime = STOPTIME;
    a4988_dir = STOP;
  }

  // setzen des Direction Output-Pins
  digitalWrite(pinDir, (a4988_dir == RIGHT));
}

void A4988::update_sumStepps() {

  if (a4988_dir == LEFT)  sumStepps -= sumStepps_multiplier * deltaStepps;
  if (a4988_dir == RIGHT) sumStepps += sumStepps_multiplier * deltaStepps;

  deltaStepps = 0;
}

long A4988::get_sumStepps() {

  update_sumStepps();
  return sumStepps;

}

void A4988::set_sumStepps(int _val) {

  deltaStepps = 0;
  sumStepps = _val;

}

float A4988::get_speed() {

  update_sumStepps();
  float _spd = ((spd_lastSumStepps - sumStepps) * sumStepps_multiplier) / ((millis() - spd_lastTime) * 3.2);
  spd_lastTime = millis();
  spd_lastSumStepps = sumStepps;

  return _spd;
}

void A4988::set_StepperResolution(int8_t res) {

  update_sumStepps();

  switch (res) {
    case FULL_STEP: sumStepps_multiplier = 16;       break;
    case HALF_STEP: sumStepps_multiplier = 8;       break;
    case QUARTER_STEP: sumStepps_multiplier = 4;    break;
    case EIGHT_STEP: sumStepps_multiplier =  2;     break;
    case SIXTEENTH_STEP: sumStepps_multiplier = 1; break;
    default: sumStepps_multiplier  = 1;             break;
  }

}

ISR (TIMER4_COMPA_vect)
{
  if (a4988_waitTime == STOPTIME) return;

  if (ISR_i++ >= a4988_waitTime) {
    ISR_i = 0;
    if (PORTE & (1 << PE5)) {
      PORTE &= ~(1 << PE5); // Aus
    } else {
      deltaStepps++;
      PORTE |= (1 << PE5);  // An
    }
  }
}


