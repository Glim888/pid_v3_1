#ifndef __A4988__
#define __A4988__

#include <Arduino.h>
#include "PID_v3.h"

#define STOPTIME 65535 // Gibt an, das keine Stepps ausgeführt werden sollen

enum Directions {
  STOP = 100,
  LEFT = 101,
  RIGHT = 102
};

class A4988 {

  private:
    int a4988_speed = 0;
    int a4988_dir = STOP;
    int a4988_i = 0;

    int pinDir = 0;
    int pinStepps = 0;
    
    // entfernte Schritte vom Nullpunkt
    long sumStepps = 0;
    int sumStepps_multiplier = 1;
    void update_sumStepps();

    long spd_lastTime = millis();
    long spd_lastSumStepps = 0;

  public:

    A4988 (int pinDir, int pinStepps);

    // initialisieren der Motoren
    void init();

    // entfernte Schritte vom Nullpunkt erhalten
    long get_sumStepps();

    void set_StepperResolution(int8_t res);

    // entfernte Schritte vom Nullpunkt setzen
    void set_sumStepps(int _val);

    // Geschwindigkeit bekommen
    float get_speed();
  
    //spd in stepps pro sek
    //dir -> Directions Enum
    void setMove(int spd, int dir);
};


#endif
