/*
   @author Lukas Oertel

   Projekt der EAT15/1 (BI Chemnitz)

   --PID Robot--
*/


#ifndef __PID_v3__
#define __PID_v3__

#include "MPU_6050_v2.h"
#include "A4988.h"
#include "PID.h"
#include <Serial_Input.h>
#include <EEPROM.h>

// Speicheradressen mit Reserven, da 4kB EEPROM
enum eeprom {

  enum_offset_acc_x = 0,
  enum_offset_acc_y = 10,
  enum_offset_acc_z = 20,
  enum_offset_gy_x = 30,
  enum_offset_gy_y = 40,
  enum_offset_gy_z = 50,

};

// STEPWIDTH
#define FULL_STEP 0x00
#define HALF_STEP 0x01
#define QUARTER_STEP 0x02
#define EIGHT_STEP 0x03
#define SIXTEENTH_STEP 0x07

// I/O
#define VOLTAGE A5
#define SCHRAUBKLEMME1 A6
#define SCHRAUBKLEMME2 A7
#define SCHRAUBKLEMME3 A8
#define SCHRAUBKLEMME4 A9
#define MS1 7
#define MS2 6
#define MS3 5
#define LED1 8
#define LED2 9
#define LED3 10
#define LED4 11

#define STEPPER_EN 13
#define STEPPER_STEP 3
#define STEPPER1_DIR A14
#define STEPPER2_DIR 4

#define MOTOR_SPD_FILTER 0.95 // Complimentary Filter für MotorSpeed 
#define UPDATE_TIME_RECV 500 // (millis)
#define UPDATE_TIME_SEND_ALL 2000 // (millis)
#define UPDATE_TIME_SEND_CORE 100 // (millis)

// DEFAULT Werte
#define DEFAULT_USE_POWER_MODE 1
#define DEFAULT_STEPPER_RESOLUTION EIGHT_STEP
#define DEFAULT_PID_UPDATETIME 20
#define DEFAULT_STEPPER_P 2500.0f
#define DEFAULT_STEPPER_I 15000.0f
#define DEFAULT_STEPPER_D -50.f
#define DEFAULT_STEPPER_LIMIT 12000.0f
#define DEFAULT_SETPOINT_P 0.5f
#define DEFAULT_SETPOINT_I 0.45f
#define DEFAULT_SETPOINT_D 0.0f
#define DEFAULT_SETPOINT_LIMIT 3.0f
#define DEFAULT_ACC_X 350.0f
#define DEFAULT_ACC_Y 529.0f
#define DEFAULT_ACC_Z 1568.0f
#define DEFAULT_GYR_X -374.0f
#define DEFAULT_GYR_Y -503.0f
#define DEFAULT_GYR_Z -266.0f

#endif
