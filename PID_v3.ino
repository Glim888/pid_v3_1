#include "PID_v3.h"

/*

   UpdateTime: 20
   stepper_P -> 2500
   stepper_I -> 15000
   stepper_D -> -50
   stepper_setpoint Drift 0.0005
   EIGHT_STEP

   setpoint_P -> 0.5
   setpoint_I -> 0.45
   setpoint_D -> 0
   setpoint_limit -> 3

*/


// speichert die letzte Zykluszeit
long lastTime_balance = millis(), lastTime_recv = millis(), lastTime_send_core = millis() , lastTime_send_all = millis();

// Zähler für das Senden
int send_i = 0;

// Motorgeschwindigkeit LOWPASS Filter
float motor_speed = 0;

// Softstart
long millisOffset = 0;

// Funktionspointer auf den Start des Programmes -> Softwarereset
void(* util_softwareReset) (void) = 0;

////A4988////
A4988 a4988_l(STEPPER1_DIR, STEPPER_STEP);
A4988 a4988_r(STEPPER2_DIR, STEPPER_STEP);

////MPU6050////
MPU_6050_v2 mpu;

////PID////
float stepper_setpoint = 0, setpoint_setpoint = 0;
pid pid_stepper(DEFAULT_STEPPER_P, DEFAULT_STEPPER_I, DEFAULT_STEPPER_D, &stepper_setpoint, DEFAULT_STEPPER_LIMIT);
pid pid_setpoint(DEFAULT_SETPOINT_P, DEFAULT_SETPOINT_I, DEFAULT_SETPOINT_D, &setpoint_setpoint, DEFAULT_SETPOINT_LIMIT * 0.15); // * 0.15 -> Softstart

////Serial////
Serial_Input si(&Serial3);
String data;

void setup() {

  Serial.begin(9600);
  Serial3.begin(9600);
  delay(30);
  Serial3.println("Bluetooth init...done");

  // I/O
  pinMode(VOLTAGE, INPUT);
  pinMode(SCHRAUBKLEMME1, INPUT);
  pinMode(SCHRAUBKLEMME2, INPUT);
  pinMode(SCHRAUBKLEMME3, INPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(MS3, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(STEPPER_EN, OUTPUT);

  // Offsets aus EEPROM laden
  mpu.loadOffsetsFromEEPROM();
  mpu.init();
  a4988_l.init();
  a4988_r.init();
  util_setStepperState(false);
  util_setStepperResolution(DEFAULT_STEPPER_RESOLUTION);

  // Wenn der Roboter gerade steht, dann wird die Endlosschleife verlassen & das Balancieren kann beginnen
  // Gyrowinkel wird auf Acc Winkel gesetzt
  do {
    mpu.get_data();
  } while (abs(mpu.get_acc_angle()) > 0.01);

  mpu.get_data();
  mpu.set_gyro_angle(mpu.get_acc_angle());
  util_setStepperState(true);
  millisOffset = millis();

}

void loop() {

  // mpu.test();

  // Bluetooth Connection
  bt_recv();
  //if (millis() - lastTime_recv > UPDATE_TIME_RECV) bt_recv();
  if (millis() - lastTime_send_all > UPDATE_TIME_SEND_ALL)  bt_send(true);
  if (millis() - lastTime_send_core > UPDATE_TIME_SEND_CORE) bt_send(false);

  mpu.get_data();

  // Balancing
  if (millis() - lastTime_balance > DEFAULT_PID_UPDATETIME) {
    lastTime_balance = millis();

    float _angle = mpu.get_angle();
    bool _b = abs(_angle) > 30;

    if (_b) util_softwareReset();
    balancing(_angle);

  }
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/*
   Balancing
*/
float debug_i = 0;
void balancing (float _angle) {

  //setpoint_setpoint = sin (debug_i);
  //debug_i += 0.013;

  // setpoint PID Limit setzen (Softstart)
  if (millis() - millisOffset > 2000) pid_setpoint.set_limit(DEFAULT_SETPOINT_LIMIT);

  // Lowpass Filter
  motor_speed = MOTOR_SPD_FILTER * motor_speed + (1 - MOTOR_SPD_FILTER) * a4988_l.get_speed();

  // PID Routine
  stepper_setpoint = pid_setpoint.calc(-motor_speed);
  float _output = pid_stepper.calc(_angle);

  // LED Visualisierung
  bool _b1 = _output > pid_stepper.get_limit() * 0.01;
  bool _b2 = _output < -pid_stepper.get_limit() * 0.01;

  digitalWrite(LED3, _b2 || !(_b1 && _b2));
  digitalWrite(LED4, _b1 || !(_b1 && _b2));

  // Stromsparmodus, falls gewollt
  util_setStepperState(DEFAULT_USE_POWER_MODE || _b1 || _b2);

  // Schrittweite setzen
  if (abs(_output) > pid_stepper.get_limit() * 0.75) {
    _output *= 0.125;
    util_setStepperResolution (FULL_STEP);
  } else if (abs(_output) > pid_stepper.get_limit() * 0.5) {
    _output *= 0.25;
    util_setStepperResolution (HALF_STEP);
  } else if (abs(_output) > pid_stepper.get_limit() * 0.25) {
    _output *= 0.5;
    util_setStepperResolution (QUARTER_STEP);
  } else {
    util_setStepperResolution (EIGHT_STEP);
  }

  // Motor ansteuern
  if (_output > 0) {
    a4988_l.setMove(abs(_output), LEFT);
    a4988_r.setMove(abs(_output), RIGHT);
  } else if (_output < 0) {
    a4988_l.setMove(abs(_output), RIGHT);
    a4988_r.setMove(abs(_output), LEFT);
  } else {
    a4988_l.setMove(0, STOP);
    a4988_r.setMove(0, STOP);
  }

}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/*
   Serial & Funktionen, welche über Serial gesteuert werden
*/

// Senden von Bluetooth Daten
void bt_send(bool _all) {

  if (_all) lastTime_send_all = millis();
  if (!_all) lastTime_send_core = millis();

  if (_all) {
    /* Anpassen was gesendet werden soll
      Serial3.print(":_PID_:");       Serial3.print(pid_setpoint.get_P()); Serial3.print("_"); Serial3.print(pid_setpoint.get_I()); Serial3.print("_"); Serial3.print(pid_setpoint.get_D()); Serial3.print("\n");
      Serial3.print(":_RT_:");        Serial3.print(millis() * 0.001); Serial3.print("\n");
      Serial3.print(":_LIMIT_:");     Serial3.print(pid_stepper.get_limit()); Serial3.print("\n");
      Serial3.print(":_VOLT_:");      Serial3.print(map(analogRead(VOLTAGE), 0, 1024, 0, 50)); Serial3.print("\n");
    */

  } else {

    //Serial3.print(":_ANG_:");       Serial3.print(mpu.get_angle()); Serial3.print("\n");
    //Serial3.print(":_SP_:");        Serial3.print(stepper_setpoint); Serial3.print("\n");

  }
}



// Empfangen von Bluetooth Daten
void bt_recv() {

  //lastTime_recv = millis() - 1000;
  // data = si.getSerial();
  if (Serial3.available()) {
    byte b[1];
    Serial3.readBytes(b, 1);
    if (b[0] == 128) setpoint_setpoint += 0.5;
    if (b[0] == 64) setpoint_setpoint -= 0.5;
    if (b[0] == 32) setpoint_setpoint = 0;
    Serial3.println(b[0], BIN);

    /*
        if (data != "NULL") {
          //String _cmd = si.getCmd(data, 3);
          String _cmd = "";
          if (_cmd == "MOV") bt_mov(data);
          if (_cmd == "RES") util_softwareReset();
          if (_cmd == "CAL") mpu.calibrate(-G_IN_VALUE, 0, 0);
          if (_cmd == "EEP") utils_eeprom_reset();

        }
    */
  }

}

void bt_tune_stepper(String _data) {

  pid_stepper.set_pid(si.getData(_data, 4, 4),
                      si.getData(_data, 9, 5),
                      si.getData(_data, 15, 5) / 100);

}

void bt_tune_setpoint(String _data) {

  float _p = si.getData(_data, 4, 4) * 0.001;
  float _i = si.getData(_data, 9, 5) * 0.001;
  float _d = si.getData(_data, 15, 5);

  pid_setpoint.set_pid( _p, _i, _d);

}

void bt_mov (String _data) {

  int _d = si.getData(_data, 4, 1);
  int _spd = si.getData(_data, 6, 4);
  int _dir_l = 0;
  int _dir_r = 0;

  switch (_d) {
    case 0: _dir_l = STOP; _dir_r = STOP; break;
    case 1: _dir_l = LEFT; _dir_r = RIGHT; break;
    case 2: _dir_l = RIGHT; _dir_r = LEFT; break;
  }

  a4988_l.setMove(_spd, _dir_l);
  a4988_r.setMove(_spd, _dir_r);

}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/*
   Utils
*/

void util_setStepperResolution(int8_t _stepperResolution) {

  a4988_l.set_StepperResolution(_stepperResolution);

  digitalWrite(MS1, _stepperResolution & (1 << 0));
  digitalWrite(MS2, _stepperResolution & (1 << 1));
  digitalWrite(MS3, _stepperResolution & (1 << 2));

}

void util_setStepperState(bool _state) {

  digitalWrite(STEPPER_EN, !_state);

}

/*
   alle EEPROM Adressen nullen
*/
void utils_eeprom_hardreset() {

  for (int i = 0; i < 100; i++) EEPROM[i] = 0;

}

/*
  zurücksetzen auf Defaultwerte
*/
void utils_eeprom_reset() {

  utils_eeprom_hardreset();

  EEPROM.put(enum_offset_acc_x,        DEFAULT_ACC_X);
  EEPROM.put(enum_offset_acc_y,        DEFAULT_ACC_Y);
  EEPROM.put(enum_offset_acc_z,        DEFAULT_ACC_Z);
  EEPROM.put(enum_offset_gy_x,         DEFAULT_GYR_X);
  EEPROM.put(enum_offset_gy_y,         DEFAULT_GYR_Y);
  EEPROM.put(enum_offset_gy_z,         DEFAULT_GYR_Z);

  delay(30);

  util_softwareReset();
}


