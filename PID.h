#ifndef __PID__
#define __PID__

#include <Arduino.h>


class pid {

  private:

    float kp, ki, kd, *setpoint, last_d = 0, sum_i = 0, limit_minMax = -1000, lastTime = millis();

  public:
    pid (float _kp, float _ki, float _kd, float *_setpoint, float _limit);

    float calc(float _angle);

    float get_P();
    float get_I();
    float get_D();
    float get_limit();

    void set_pid(float _p, float _i, float _d);
    void set_P(float _p);
    void set_I(float _i);
    void set_D(float _d);
    void set_limit(float _minMax);

};
#endif
